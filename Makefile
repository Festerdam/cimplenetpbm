all : pbmtools.c
	gcc -O2 -fPIC -shared -o libcimplenetpbm.so pbmtools.c

debug : pbmtools.c test.c
	gcc -g -fPIC -Wall -Wextra -shared -std=c11 -pedantic -o libcimplenetpbm.so pbmtools.c
	gcc -g -std=c11 -pedantic -o test test.c libcimplenetpbm.so

clean :
	rm -f libcimplenetpbm.so test test.pbm

