/*
    Copyright (C) 2018 Festerdam

    This file is part of cimplenetpbm.

    cimplenetpbm is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cimplenetpbm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cimplenetpbm.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef pbmtools
#define pbmtools

#include <stdint.h>

#define COLOR_WHITE '0'
#define COLOR_BLACK '1'


void P1_blank_pixmap(char **pixmap, uintmax_t width, uintmax_t height, char color);

void P1_draw_line(char **pixmap, uintmax_t x1, uintmax_t y1, uintmax_t x2, uintmax_t y2, char color);

char **P1_generate_pixmap(uintmax_t width, uintmax_t height);

uintmax_t P1_calculate_img_size(uint_fast8_t header_size, uintmax_t width, uintmax_t height);

char *P1_generate_header(uintmax_t width, uintmax_t height);

char *P1_create_img(uintmax_t width, uintmax_t height);

char *P1_convert_pixmap_to_img(char **pixmap, uintmax_t width, uintmax_t height);

#endif
