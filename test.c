/*
    Copyright (C) 2018 Festerdam

    This file is part of cimplenetpbm.

    cimplenetpbm is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cimplenetpbm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cimplenetpbm.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
This file is intended to be used to test libcimplenetpbm.
Return values:
0 - OK
1 - Wrong number of arguments
*/

#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "cimplenetpbm.h"

int main(int argc, char **argv) {
  uintmax_t width = 255;
  uintmax_t height = 255;
  uintmax_t size_of_file;
  FILE *file_buffer;
  char **image_pixmap;
  char *image_head;
  char *generated_image;

  image_head = P1_generate_header(width, height);
  if (image_head == NULL) {
    fprintf(stderr, "An error occured while generating the header.");
    return 1;
  }
  size_of_file = P1_calculate_img_size(strlen(image_head), width, height);
  image_pixmap = P1_generate_pixmap(width, height);
  if (image_pixmap == NULL) {
    fprintf(stderr, "An error occured while generating the pixmap.");
  }
  P1_blank_pixmap(image_pixmap, width, height, COLOR_BLACK);
  P1_draw_line(image_pixmap, 10, 100, 100, 80, COLOR_WHITE);
  P1_draw_line(image_pixmap, 100, 122, 10, 254, COLOR_WHITE);
  P1_draw_line(image_pixmap, 9, 252, 100, 122, COLOR_WHITE);
  P1_draw_line(image_pixmap, 25, 100, 43, 2, COLOR_WHITE);
  P1_draw_line(image_pixmap, 50, 50, 50, 200, COLOR_WHITE);
  P1_draw_line(image_pixmap, 25, 75, 200, 100, COLOR_WHITE);
  P1_draw_line(image_pixmap, 25, 75, 200, 75, COLOR_WHITE);
  generated_image = P1_convert_pixmap_to_img(image_pixmap, width, height);
  if (generated_image == NULL) {
    fprintf(stderr, "An error accured while converting the pixmap to an image.");
  }

  printf("width = %" PRIuMAX "\n", width);
  printf("height = %" PRIuMAX "\n", height);
  printf("size_of_file = %" PRIuMAX "\n", size_of_file);
  printf("image_head:\n%s", image_head);
  printf("generated_image:\n%s", generated_image);
  puts("Writing image to disk...");
  file_buffer = fopen("test.pbm", "w");
  if (file_buffer == NULL) {
    fprintf(stderr, "An error occured while opening test.pbm .\n");
  }
  if (fwrite(generated_image, sizeof(char), strlen(generated_image), file_buffer) < strlen(generated_image)) {
    fprintf(stderr, "An error occured while writing to test.pbm .\n");
  }
  puts("Done. Generated image saved to ./test.pbm .");
  puts("Closing buffers...");
  fclose(file_buffer);
  free(generated_image);
  free(image_pixmap);
  free(image_head);
  puts("Done.");

  return 0;
}

