/*
    Copyright (C) 2018 Festerdam

    This file is part of cimplenetpbm.

    cimplenetpbm is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cimplenetpbm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cimplenetpbm.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <inttypes.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define COLOR_WHITE '0'
#define COLOR_BLACK '1'


inline void P1_set_pixel_at_coordinates(char **pixmap, uintmax_t x, uintmax_t y, char color) {
  /*
  This function allows coordinates of an pixmap to be written as (x;y). Where y
  grows downward.
  */
  pixmap[y][x] = color;
}


//Should the color be supplied as a bool or as a char??
void P1_blank_pixmap(char **pixmap, uintmax_t width, uintmax_t height, char color) {
  /*
  This function sets all pixels in a pixmap to the supplied color.
  It requires a pixmap and a color(1 for black, 0 for white) to be given
  as arguments.
  */

  for (uintmax_t row = 0; row < height; row++) {
    for (uintmax_t column = 0; column < width; column++) {
      pixmap[row][column] = color;
    }
  }
}


void P1_draw_line(char **pixmap, uintmax_t x_start, uintmax_t y_start, uintmax_t x_end, uintmax_t y_end, char color) {
  /*
  Complete this...
  */
  intmax_t x0, y0, x1, y1;

  if (x_start > x_end) {
    x0 = x_end;
    y0 = y_end;
    x1 = x_start;
    y1 = y_start;
  } //Who cares about which way we draw?
  else{
    x0 = x_start;
    y0 = y_start;
    x1 = x_end;
    y1 = y_end;
  }

  intmax_t deltax = x1 - x0;
  intmax_t deltay = y1 - y0;
  intmax_t abs_deltax = abs(deltax);
  intmax_t abs_deltay = abs(deltay);

  //1 > slope > 0 or 0 > slope > -1
  if (abs_deltay < deltax) {
    //intmax_t deltax_m = deltay; //delta_x multiplied by m. m is the proportionality
                                //constant.
    int_fast8_t deltay_sign = (deltay > 0) - (deltay < 0);
    intmax_t deltax_error = 0;

    intmax_t current_x = x0;
    intmax_t current_y = y0;
    for (; current_x <= x1; current_x++) {
      pixmap[current_y][current_x] = color;
      if (2 * deltax_error > deltax) {
        current_y += deltay_sign;
        deltax_error -= abs_deltax;
      }
      deltax_error += abs_deltay;
    }
  }
  //+inf > slope > 1 or -1 > slope > -inf
  //I know that vertical lines don't have a slope.
  else {

    int_fast8_t deltax_sign = (deltax > 0) - (deltax < 0);
    int_fast8_t deltay_sign = (deltay > 0) - (deltay < 0);
    intmax_t deltay_error = 0;

    intmax_t current_x = x0;
    intmax_t current_y = y0;
    for (;; current_y += deltay_sign) {
      pixmap[current_y][current_x] = color;
      if (current_y == y1) {
        break;
      }
      if (2 * deltay_error > deltay) {
        current_x += deltax_sign;
        deltay_error -= abs_deltay;
      }
      deltay_error += abs_deltax;
    }
  }
}


char **P1_generate_pixmap(uintmax_t width, uintmax_t height) {
  /*
  This function allocates memory for the pixmap and returns a pointer to it(NULL
  if it didn't succeed).
  It requires width and height in pixels to be given as arguments.
  */

  char **pixmap = malloc(height * sizeof(char *));
  if (pixmap == NULL) {
    return NULL;
  }
  for (uintmax_t index = 0; index < height; index++) {
    pixmap[index] = malloc(width * sizeof(char));

    //Not sure if these checks will slow the generation down too much...
    if (pixmap[index] == NULL) {
      return NULL;
    }
  }

  return pixmap;
}


//Todo: Not sure if also P4 could be used...
uintmax_t P1_calculate_img_size(uint_fast8_t header_size, uintmax_t width, uintmax_t height) {
  /*
  This function calculates and returns the size of a P1 image in bytes.
  It requires header size in bytes, width and height in pixels to be supplied
  as arguments.
  */

  // size of image = header_size + size_of_content
  // size of content = height * (2 * width)

  uintmax_t size_of_image = height * (width << 1) + header_size;

  return size_of_image;
}


//Todo: Make compatible with P4. Headers are ascii in P4 IIRC.
char *P1_generate_header(uintmax_t width, uintmax_t height) {
  /*
  This function generates a P1 header and returns a pointer to it(NULL if it
  didn't succeed.
  It requires width and height in pixels to be supplied as arguments.
  */

  char *header = malloc(46); //20 + 20 + 3 + 2 + 1
  if (header == NULL) {
    return NULL;
  }

  //Put % near P to make it possible for P4 or other formats...
  sprintf(header, "P1 %" PRIuMAX " %" PRIuMAX "\n", width, height);
  header = realloc(header, strlen(header + 1)); //+1 just to know where it ends.
  if (header == NULL) {
    return NULL;
  }

  return header;
}


char *P1_create_img(uintmax_t width, uintmax_t height) {
  /*
  This functions allocates memory and writes P1 header to beggining and returns
  a pointer to it(NULL if it didn't succeed).
  It requires width and height in pixels to be given as arguments.
  */

  char *image = P1_generate_header(width, height);
  uintmax_t image_size = P1_calculate_img_size(strlen(image), width, height);


  image = realloc(image, image_size);
  if (image == NULL) {
    return NULL;
  }

  return image;
}


char *P1_convert_pixmap_to_img(char **pixmap, uintmax_t width, uintmax_t height) {
  /*
  This function converts a pixmap into a P1 image and returns a pointer to
  it(NULL if it didn't succeed).
  It requires a pixmap and width and height in pixels to be given as arguments.
  */

  //We're generating the header of the image twice. That should be fixed.
  char *image_header = P1_generate_header(width, height);
  char *image = P1_create_img(width, height);
  if (image == NULL || image_header == NULL) {
    return NULL;
  }
  char *image_content_start = image + strlen(image_header);
  free(image_header);

  for (uintmax_t row = 0, image_index = 0; row < height; row++) {
    for (uintmax_t column = 0; column < width; column++) {
      image_content_start[image_index] = pixmap[row][column];
      image_content_start[++image_index] = ' ';
      image_index++;
    }
  image_content_start[image_index - 1] = '\n';
  }

  return image;
}

